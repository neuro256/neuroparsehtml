﻿using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Dom.Css;
using AngleSharp.Dom.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace NeuroParseHtml
{
    public class Parser
    {
        public delegate void ParserStateHandler();
        public event ParserStateHandler Parsed;
        private readonly IBrowsingContext _context;
        private IDocument _document;
        private Task _currentTask;
        private CancellationTokenSource _cts;
        private Dictionary<string, int> _classes;
        private List<ICssStyleSheet> _sheets;
        private Dictionary<string, int> _cssStyleRules;

        public Dictionary<string, int> Classes { get => _classes; }
        public List<ICssStyleSheet> Sheets { get => _sheets;  }
        public Dictionary<string, int> CssStyleRules { get => _cssStyleRules;  }

        public Parser()
        {
            // Setup the configuration to support document loading
            var config = Configuration.Default.WithCss().WithRequesters(setup =>
            {
                setup.IsNavigationEnabled = true;
                setup.IsResourceLoadingEnabled = true;
            });
            _cts = new CancellationTokenSource();
            _context = BrowsingContext.New(config);
            _classes = new Dictionary<string, int>();
            _sheets = new List<ICssStyleSheet>();
            _cssStyleRules = new Dictionary<string, int>();
        }

        public void Parse(string address)
        {
            var url = CreateUrlFrom(address);
            if (_currentTask != null && !_currentTask.IsCompleted)
            {
                _cts.Cancel();
                _cts = new CancellationTokenSource();
            }

            _currentTask = LoadAsync(url, _cts.Token);
        }

        private async Task LoadAsync(Url url, CancellationToken cancel)
        {
            // загружает страницу по ссылке 
            _document = await _context.OpenAsync(url, cancel);
            LoadAttributeClass();
            LoadCssStyleSheets();
            LoadCssStyleRules();
            Parsed?.Invoke();
        }

        /// <summary>
        /// Создание массива наименований CSS селекторов 
        /// </summary>
        private void LoadCssStyleRules()
        {
            _cssStyleRules.Clear();

            foreach (var sheet in _sheets)
            {
                for (int i = 0; i < sheet.Rules.Length; i++)
                {
                    var rule = sheet.Rules[i];
                    if (rule.Type == CssRuleType.Style)
                    {
                        var style = (ICssStyleRule)rule;

                        if (_cssStyleRules.ContainsKey(style.SelectorText))
                            _cssStyleRules[style.SelectorText]++;
                        else
                            _cssStyleRules.Add(style.SelectorText, 1);
                    }
                }
            }
        }

        /// <summary>
        /// Загружает стили страницы
        /// </summary>
        private void LoadCssStyleSheets()
        {
            _sheets.Clear();

            if(_document != null)
            {
                _document.Context.Parsed -= ParseEnded;
            }

            foreach (ICssStyleSheet sheet in _document.StyleSheets)
            {
                _sheets.Add(sheet);
            }

            _document.Context.Parsed += ParseEnded;
        }

        private void ParseEnded(object sender, Event ev)
        {
            var data = ev as CssParseEvent;

            if(data != null)
            {
                _sheets.Add(data.StyleSheet);
            }
        }

        /// <summary>
        /// Создание массива значений атрибута class
        /// </summary>
        private void LoadAttributeClass()
        {
            var cellSelector = "*";
            var elements = _document.QuerySelectorAll(cellSelector);
            _classes.Clear();
            foreach (var elem in elements)
            {
                foreach (var cls in elem.ClassList)
                {
                    if (_classes.ContainsKey(cls))
                        _classes[cls]++;
                    else
                        _classes.Add(cls, 1);
                }
            }
        }

        private Url CreateUrlFrom(String address)
        {
            if (File.Exists(address))
            {
                address = "file://localhost/" + address.Replace('\\', '/');
            }

            var lurl = address.ToLower();

            if (!lurl.StartsWith("file://") && !lurl.StartsWith("http://") && !lurl.StartsWith("https://") && !lurl.StartsWith("data:"))
            {
                address = "http://" + address;
            }

            var url = Url.Create(address);

            if (!url.IsInvalid && url.IsAbsolute)
            {
                return url;
            }

            return Url.Create("http://www.google.com/search?q=" + address);
        }

        /*
         * Inspect(document.DocumentElement, classes);
        static void Inspect(IElement element, Dictionary<string, int> classes)
        {
            foreach (var cls in element.ClassList)
            {
                if (classes.ContainsKey(cls))
                    classes[cls]++;
                else
                    classes.Add(cls, 1);
            }

            foreach (var child in element.Children)
                Inspect(child, classes);
        }
         */
    }
}
