﻿using AngleSharp.Dom.Css;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NeuroParseHtml
{
    public partial class Form1 : Form
    {
        private Parser _parser;
        private Dictionary<string, int> _classes;
        private List<ICssStyleSheet> _sheets;
        private Dictionary<string, int> _cssStyleRules;

        public Form1()
        {
            InitializeComponent();
            _parser = new Parser();
            _parser.Parsed += ParseEnded;
            _classes = new Dictionary<string, int>();
            _sheets = new List<ICssStyleSheet>();
            _cssStyleRules = new Dictionary<string, int>();
        }

        private void ParseEnded()
        {
            _classes = _parser.Classes;
            _sheets = _parser.Sheets;
            _cssStyleRules = _parser.CssStyleRules;
            ShowClasses();
            ShowCssStyleRules();
            ShowStyleSheets();
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            _parser.Parse(richTextBoxLink.Text);
        }

        private void ShowClasses()
        {
            listView1.BeginUpdate();
            listView1.Items.Clear();
            foreach(var cl in _classes)
            {
                ListViewItem item = listView1.Items.Add(cl.Key);
                item.SubItems.Add(cl.Value.ToString());
            }
            listView1.EndUpdate();
        }
        
        private void ShowStyleSheets()
        {
            listView3.BeginUpdate();
            listView3.Items.Clear();
            foreach(var sheet in _sheets)
            {
                listView3.Items.Add(sheet.Href ?? "<inlined>");
            }
            listView3.EndUpdate();
        }

        private void ShowCssStyleRules()
        {
            listView2.BeginUpdate();
            listView2.Items.Clear();
            foreach (var rule in _cssStyleRules)
            {
                ListViewItem item = listView2.Items.Add(rule.Key);
                item.SubItems.Add(rule.Value.ToString());
            }
            listView2.EndUpdate();
        }
    }
}
